<!-- array_diff_assoc compare the key and values of two array and return the difference -->
<?php
$a1 = ['a'=>'red','b'=>'yellow','c'=>'pink'];
$a2 = ['a'=>'orange','b'=>'yellow','c'=>'red'];
$result = array_diff_assoc($a1, $a2);
print_r($result);